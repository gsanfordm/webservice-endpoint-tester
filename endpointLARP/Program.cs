﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace mockEndpoint
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Start");

            // mock LARP Request payload 
            ConduentNotificationRequest larp = new ConduentNotificationRequest
            {
                ProgramName = "SA1M0M",
                TransId = "SAMM",
                UserId = "SPA035",
                SessionId = "1234567",
                Password = "ASZX129V",
                InternetIndicator = "I",
                //SystemIndicator = "I",
                Token = "10",
                ClientCode = "17",
                FunctionCode = "01",
                ErrorCode = "",
                Account = "1",
                PaymentMethod = "4",
                TotalPaid = "110.00",
                ProcessingFee = "",
                PayDate = DateTime.Now.ToString("MM/dd/yyyy"),//"07/28/2018",
                PayTime = DateTime.Now.ToString("HH:mm tt").Replace(" ", ""), //"07:40AM",
                CardType = "",
                CardNumber = "1234",
                AuthCode = "",
                TicketCount = "1",
                TicketList = new List<ConduentTicketItem>()
            };
            ConduentTicketItem ticket1 = new ConduentTicketItem
            {
                AmountPaid = "110.00",
                EntityNumber = "0183434",
                FeeDate = "07/01/2018",
                FeeTicketIndicator = "T",
                FeeTime = "08:10AM",
                FeeType = "",
                TicketNumber = "3102036254"
            };
            ConduentTicketItem ticket2 = new ConduentTicketItem
            {
                AmountPaid = "90.00",
                EntityNumber = "1",
                FeeDate = "07202018",
                FeeTicketIndicator = "T",
                FeeTime = "0900",
                FeeType = "001",
                TicketNumber = "126739"
            };
            ConduentTicketItem ticket3 = new ConduentTicketItem
            {
                AmountPaid = "120.00",
                EntityNumber = "2",
                FeeDate = "07202018",
                FeeTicketIndicator = "T",
                FeeTime = "0900",
                FeeType = "001",
                TicketNumber = "126740"
            };
            ConduentTicketItem ticket4 = new ConduentTicketItem
            {
                AmountPaid = "10.00",
                EntityNumber = "4",
                FeeDate = "07202018",
                FeeTicketIndicator = "F",
                FeeTime = "0900",
                FeeType = "004",
                TicketNumber = "126741"
            };
            ConduentTicketItem ticket5 = new ConduentTicketItem
            {
                AmountPaid = "91.00",
                EntityNumber = "3",
                FeeDate = "07202018",
                FeeTicketIndicator = "T",
                FeeTime = "0900",
                FeeType = "001",
                TicketNumber = "126742"
            };
            ConduentTicketItem ticket6 = new ConduentTicketItem
            {
                AmountPaid = "92.00",
                EntityNumber = "4",
                FeeDate = "07202018",
                FeeTicketIndicator = "T",
                FeeTime = "0900",
                FeeType = "001",
                TicketNumber = "126743"
            };
            ConduentTicketItem ticket7 = new ConduentTicketItem
            {
                AmountPaid = "93.00",
                EntityNumber = "4",
                FeeDate = "07202018",
                FeeTicketIndicator = "F",
                FeeTime = "0900",
                FeeType = "001",
                TicketNumber = "126744"
            };
            larp.TicketList.Add(ticket1);
            //larp.Tickets.Add(ticket2);
            string request = WebServicesXmlSerializer.XmlSerializeNoNamespaces(larp);
            string key = "Iyjnh817rdyAoMyp99tqRJKr2LtDQQ+OsG0/ZI4e0QQ=";

            System.Xml.Linq.XCData cdataRequestNode = new XCData(request);
            string cdataRequest = cdataRequestNode.ToString();

            Console.WriteLine(cdataRequest);

            string addXmlRequest = "<XmlRequest>" + cdataRequest + "</XmlRequest>";
            string addKey = "<Key>" + key + "</Key>";

            /* mock service call and response */
            MockCall mockCall = new MockCall(addXmlRequest, addKey);
            var req = mockCall.CallPaymentNotification();



            /* test ticket list logic */

            /*
            List<FakeBill> bills = new List<FakeBill>();
            FakeBill bill1 = new FakeBill
            {
                Amount = new Decimal(60.00),
                BillType = BillCategory.ParkingFine,
                DateCreated = DateTime.Now,
                ReferenceId = "2"
            };

            bills.Add(bill1);

            FakeBill bill2 = new FakeBill
            {
                Amount = new Decimal(110.00),
                BillType = BillCategory.BootFee,
                DateCreated = DateTime.Now,
                ReferenceId = "1"
            };

            bills.Add(bill2);

            FakeBill bill3 = new FakeBill
            {
                Amount = new Decimal(10.00),
                BillType = BillCategory.ReturnedCheckFee,
                DateCreated = DateTime.Now,
                ReferenceId = "3"
            };

            bills.Add(bill3);

            //ConduentPaymentNotification testLogic = new ConduentPaymentNotification();
            FakeBill fakeClass = new FakeBill();
            var ticketList = new List<ConduentTicketItem>();
            if (bills != null)
            {
                ticketList.AddRange(bills.ToArray().Map(bill => new ConduentTicketItem
                {
                    FeeTicketIndicator = fakeClass.GetFeeType(bill) == "" ? "T" : "F",
                    FeeType = fakeClass.GetFeeType(bill),
                    FeeDate = (fakeClass.GetFeeType(bill) == "") ? "" : bill.DateCreated.ToString("MM/dd/yyyy"),
                    FeeTime = (fakeClass.GetFeeType(bill) == "") ? "" : bill.DateCreated.ToString("HH:mm t").Replace(" ", ""),
                    TicketNumber = bill.ReferenceId,
                    AmountPaid = bill.Amount.ToString(),
                    EntityNumber = ""
                }));
            }

            foreach (ConduentTicketItem item in ticketList) {
                Console.WriteLine(item.FeeTicketIndicator +  " " + item.FeeType + " " + item.FeeDate + " " + item.FeeTime + " " +
                    item.TicketNumber + " " + item.AmountPaid + " " + item.EntityNumber);
            }
            */

            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
