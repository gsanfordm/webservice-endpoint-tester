﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace mockEndpoint
{
    public static class WebServicesXmlSerializer
    {
        public static string XmlSerializeNoNamespaces<T>(T entity) where T : class
        {
            // removes version
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                xsSubmit.Serialize(writer, entity, xmlns);
                return sw.ToString(); // Your XML
            }
        }

        public static string SerializeToXml<T>(this T objectToSerialize)
        {
            var xs = new XmlSerializer(typeof(T));
            var sw = new StringWriter();
            xs.Serialize(sw, objectToSerialize);
            return sw.GetStringBuilder().ToString();

        }

        public static T DeserializeFromXml<T>(this string xmlToDeserialize)
        {
            var xs = new XmlSerializer(typeof(T));
            var sr = new StringReader(xmlToDeserialize);
            var response = (T)xs.Deserialize(sr);
            return response;
        }
    }
}
