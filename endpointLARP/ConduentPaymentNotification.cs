﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Reflection;
using PayLock.BootView.Business;
using PayLock.BootView.WebServicesClient;
using PayLock.BootView.WebServicesClient.ConduentPaymentListService;
using RetryPattern;
using ServiceStack;
using PayLock.BootView.Business.WebServices;

namespace mockEndpoint
{
    public interface IPaymentNotification2
    {
        String CallPaymentNotification(ConduentNotificationRequest paymentDto);
        //void LogResponse(Payment payment, ConduentNotificationRequest request, sendPaymentDetailsResponseBodyErrorXMLResponse response, bool success);
        ConduentNotificationRequest MapPaymentToPaymentNotificationRequest(Payment payment);
        List<ConduentTicketItem> SetBillPaymentsDto(BillPaymentCollection RelatedBillsPayment);
        BillPaymentCollection SendPaymentNotification(Payment payment, out string responseMsg);
        bool SendPaymentNotification(BillPayment payment, out string responseMsg);
        void ValidatePaymentBeforeSendingToClient(Payment payment);
        String GetPaymentType(PaymentType paymentType);
    }
    public class ConduentPaymentNotification : IPaymentNotification2
    {
        private readonly PaymentListServiceClient _service;
        private readonly IRecordValidator _recordValidator;

        public ConduentPaymentNotification() : this(new RecordValidator(), new PaymentListServiceClient())
        {
        }
        public ConduentPaymentNotification(IRecordValidator recordValidator, PaymentListServiceClient paymentListServiceClient)
        {
            _recordValidator = recordValidator;
            _service = paymentListServiceClient;
        }
        public String CallPaymentNotification(ConduentNotificationRequest paymentDto)
        {

            var response = _service.sendPaymentDetails("Iyjnh817rdyAoMyp99tqRJKr2LtDQQ+OsG0/ZI4e0QQ=", paymentDto.SerializeToXml());
            return response;

        }
        public ConduentNotificationRequest MapPaymentToPaymentNotificationRequest(Payment payment)
        {
            CreditPayment creditPayment = new CreditPayment(payment.PaymentId);
            List<ConduentTicketItem> ticketList = SetBillPaymentsDto(payment.RelatedBillsPayment);

            if (ticketList.Count < 100)
            {
                var request = new ConduentNotificationRequest
                {
                    Token = "",
                    PayDate = payment.PlacedDate.ToString("MM/dd/yyyy"),
                    PayTime = payment.PlacedDate.ToString("HH:mm t").Replace(" ", ""),
                    // does this include voided tickets ??
                    TicketCount = payment.RelatedBillsPayment.Count.ToString(CultureInfo.InvariantCulture),
                    AuthCode = creditPayment.GatewayConfirmationNumber,
                    CardType = "",
                    CardNumber = creditPayment.CardNumberLastFour,
                    PaymentMethod = GetPaymentType(payment.Type),
                    ProcessingFee = "",
                    TotalPaid = payment.TotalAmount.ToString(CultureInfo.InvariantCulture),
                    TicketList = ticketList,
                    SessionId = ""
                };

                return request;
            }
            else throw new NotImplementedException(); //need to split up requests when there are more than 99 tickets
        }
        public List<ConduentTicketItem> SetBillPaymentsDtoOld(BillPaymentCollection billPayments)
        {
            var ticketList = new List<ConduentTicketItem>();

            //var paylockFeeBillTypes = new List<BillCategory>() {
            //    BillCategory.PaylockFee, BillCategory.TowFee, BillCategory.ImpoundFee, BillCategory.LateBootFee, BillCategory.DamagedBootFee,
            //    BillCategory.LostBootFee, BillCategory.ReturnedCheckFee, BillCategory.NoticeFee, BillCategory.SecurityFee,
            //    BillCategory.AdministrativeFee, BillCategory.SalesTax, BillCategory.MarshalExpense, BillCategory.CityDecal
            //};

            if (billPayments != null)
            {
                ticketList.AddRange(billPayments.ToArray().Map(billPayment => new ConduentTicketItem
                {
                    //FeeTicketIndicator = paylockFeeBillTypes.Contains(billPayment.Bill.BillType) ? "F" : "T",
                    FeeTicketIndicator = (billPayment.Bill.BillType == BillCategory.PaylockFee) ? "F" : "T",
                    FeeType = (billPayment.Bill.BillType == BillCategory.PaylockFee) ? "001" : "", //bootfee for now
                    FeeDate = (billPayment.Bill.BillType == BillCategory.PaylockFee) ? billPayment.Bill.DateCreated.ToString("d") : "", //("MM/dd/yyyy")
                    FeeTime = (billPayment.Bill.BillType == BillCategory.PaylockFee) ? billPayment.Bill.DateCreated.ToString("t") : "", //("HH:mm")
                    TicketNumber = billPayment.Bill.ReferenceId,
                    AmountPaid = billPayment.Amount.ToString(CultureInfo.InvariantCulture),
                    EntityNumber = "***GET_FROM_INQUIRY***"// TODO: wait on Conduents answer inquiryResponse.ENT-NUM
                }));
            }

            return ticketList;
        }
        public List<ConduentTicketItem> SetBillPaymentsDto(BillPaymentCollection billPayments)
        {
            var ticketList = new List<ConduentTicketItem>();

            if (billPayments != null)
            {
                ticketList.AddRange(billPayments.ToArray().Map(billPayment => new ConduentTicketItem
                {
                    FeeTicketIndicator = GetFeeType(billPayment.Bill) == "" ? "T" : "F",
                    FeeType = GetFeeType(billPayment.Bill),
                    FeeDate = (GetFeeType(billPayment.Bill) == "") ? "" : billPayment.Bill.DateCreated.ToString("MM/dd/yyyy"),
                    FeeTime = (GetFeeType(billPayment.Bill) == "") ? "" : billPayment.Bill.DateCreated.ToString("HH:mm t").Replace(" ", ""),
                    TicketNumber = billPayment.Bill.ReferenceId,
                    AmountPaid = billPayment.Amount.ToString(CultureInfo.InvariantCulture),
                    EntityNumber = GetEntityNumber(billPayment.Bill) ?? ""
                }));
            }

            return ticketList;
        }

        public String GetEntityNumber(Bill bill)
        {
            return bill.Vehicle.EntityNumber;
        }

        public String GetFeeType(Bill bill)
        {
            switch (bill.BillType)
            {
                case BillCategory.BootFee:
                    return "001";
                case BillCategory.ReturnedCheckFee:
                    return "005";
                case BillCategory.PaylockFee:
                    return "001";
                default:
                    return "";
            }
        }

        public String GetPaymentType(PaymentType paymentType)
        {
            switch (paymentType.Code)
            {
                case PaymentType.CREDIT_CODE:
                    return "4";
                case PaymentType.DEBIT_CODE:
                    return "5";
                case PaymentType.CHECKING_CODE:
                    return "2";
                case PaymentType.CASH_CODE:
                    return "1";
                default:
                    return "";
            }
        }

        public BillPaymentCollection SendPaymentNotification(Payment payment, out string responseMsg)
        {
            //TODO: fix up
            responseMsg = "";
            return null;
        }

        public bool SendPaymentNotification(BillPayment billPayment, out string responseMsg)
        {
            //TODO: fix up
            responseMsg = "";
            try
            {
                _recordValidator.ValidatePaymentRecord(billPayment.Payment);
            }
            catch (Exception ex)
            {
                //_log.PublishWarn(string.Format("paymentNotification.noNotification.Conduent payment failed validation. exception = {0}",
                //    ex.Message));
                return false;
            }

            //TODO: setup and hit service



            //TODO: fix
            return true;
        }
        public BillPaymentCollection SendPaymentNotificationOld(Payment payment, out string response)  // //BillPaymentCollection billPayments
        {
            response = "";
            var sentPayments = new BillPaymentCollection();
            var paymentNotificationRequest = new ConduentNotificationRequest();
            var sendPaymentDetailsResponseBodyErrorXMLResponse = new sendPaymentDetailsResponseBodyErrorXMLResponse();
            //validate before attempting to send the payment
            try
            {
                ValidatePaymentBeforeSendingToClient(payment);
            }
            catch (Exception ex)
            {
                //_log.PublishWarn("Conduent Payment Notification - validation failure. exception" + ex.Message);
                return sentPayments;
            }

            try
            {
                // TODO: fix  The magic happens here :)
                if (payment.RelatedBillsPayment.Count > 0)
                {
                    paymentNotificationRequest = MapPaymentToPaymentNotificationRequest(payment);
                    //sendPaymentDetailsResponseBodyErrorXMLResponse = CallPaymentNotification(paymentNotificationRequest);

                    if (sendPaymentDetailsResponseBodyErrorXMLResponse == null)
                    {
                        //LogResponse(payment, paymentNotificationRequest, null, false);
                        response = "No response returned";
                        return sentPayments;
                    }

                    //TODO Fix:
                    //bool isSuccessful = sendPaymentDetailsResponseBodyErrorXMLResponse.ErrorMessage.Length == 0;
                    bool isSuccessful = true;
                    if (isSuccessful)
                    {
                        //response = sendPaymentDetailsResponseBodyErrorXMLResponse.ReceiptNumber;
                        //payment.CustomerReceiptDate = DateTime.Today;
                        //payment.CustomerReceiptNumber = sendPaymentDetailsResponseBodyErrorXMLResponse.ReceiptNumber;
                        //payment.Save();

                        //LogResponse(payment, paymentNotificationRequest, sendPaymentDetailsResponseBodyErrorXMLResponse, true);
                        //sentPayments = payment.RelatedBillsPayment;
                        //response = " Successfully Notified of Payment(s) ";
                    }
                    else
                    {
                        //_log.PublishWarn("Non successful response received");
                        response = " Unable to send payment: "; //TODO: Fix + sendPaymentDetailsResponseBodyErrorXMLResponse.ErrorMessage;
                        //LogResponse(payment, paymentNotificationRequest, sendPaymentDetailsResponseBodyErrorXMLResponse, false);
                    }
                }

                return sentPayments; //the caller expects a collection if the collection count is 0, the caller assumes failure           
            }
            catch (Exception ex)
            {
                //_log.PublishError("Unable to send payment. Exception:" + ex.Message);
                response = "Unable to send payment. Exception:" + ex.Message;
                //LogResponse(payment, paymentNotificationRequest, sendPaymentDetailsResponseBodyErrorXMLResponse, false);
                return sentPayments;
            }
        }

        public void ValidatePaymentBeforeSendingToClient(Payment payment)
        {
            _recordValidator.ValidatePaymentRecord(payment);
        }
    }
}
