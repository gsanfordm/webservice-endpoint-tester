
Simple Web Service Endpoint Tester 

Purpose:
Speed up time in new partner integraton to simply insure the endpoint can be hit with proper test payload
in actual code, one step beyond just using POSTMAN

To help flesh out partner specific soap issues in a quick fashion. The sooner this is done we can implement in web services with a working data flow

This is lightweight and not connected to BootView, just mock you data objects in a simple manner
Simply for creating mock XML elements with the raw test values that succeed in POSTMAN


Requirements:
Partner must have a QA endpoint with valid data to test against
You need to insure you have a POSTMAN post working and returning the data from the Partner


Usage:
1. Add Service Reference
2. Add a mock request class for the new service with the actual operation that posts
3. build out the test request object with the data and call it



