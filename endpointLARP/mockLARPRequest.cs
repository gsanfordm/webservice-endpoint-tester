﻿using mockEndpoint.ConduentPaymentListService;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml.Serialization;

namespace mockEndpoint
{
    [ServiceContract(Namespace = "http://base.wsdl.wss.muni.acs.com")]
    [XmlRoot("PaymentListService")]
    public class ConduentNotificationRequest
    {
        [XmlElement("PROGRAM-NAME", typeof(string))]
        public string ProgramName = "SA1M0M";

        [XmlElement("TRANS-ID", typeof(string))]
        public string TransId = "SAMM";

        [XmlElement("USERID", typeof(string))]
        public string UserId = "SPA035";

        [XmlElement("SESSION-ID", typeof(string))]
        public string SessionId = String.Empty; //NUM 7 Random number, not required

        [XmlElement("INTERNET-IND", typeof(string))]
        public string InternetIndicator = "I";

        //[XmlElement("SYSTEM-IND", typeof(string))]
        //public string SystemIndicator = "I";

        [XmlElement("TOKEN", typeof(string))]
        public string Token; //***GET_FROM_INQUIRY***

        [XmlElement("FUNCTION-CODE", typeof(string))]
        public string FunctionCode = "01";

        [XmlElement("CLIENT-CODE", typeof(string))]
        public string ClientCode = "17";

        [XmlElement("ERROR-CODE", typeof(string))]
        public string ErrorCode; //CHAR 4

        [XmlElement("PASSWORD", typeof(string))]
        public string Password = "ASZX129V";

        [XmlElement("ACCOUNT", typeof(string))]
        public string Account = "1"; //CHAR 4 Account Number(This will always be 6)

        [XmlElement("PAYMENT-METHOD", typeof(string))]
        public string PaymentMethod; //CHAR 2 CA-CASH, CC-Credit card, DC-Debit card, CK-check

        [XmlElement("TOTAL-PAID", typeof(string))]
        public string TotalPaid; //NUM 9 Paid Amount, e.g. 9999999.99

        [XmlElement("PROC-FEE", typeof(string))]
        public string ProcessingFee; //NUM 7 Processing fee, e.g. 99999.99

        [XmlElement("PAY-DATE", typeof(string))]
        public string PayDate; //DATE 8 The format should be mmDDyyyy

        [XmlElement("PAY-TIME", typeof(string))]
        public string PayTime; //TIME 4 The format should be hhmm military time

        [XmlElement("CARD-TYPE", typeof(string))]
        public string CardType; //CHAR 2 This is the first two numbers of the credit card.

        [XmlElement("CARD-NUMBER", typeof(string))]
        public string CardNumber; //CHAR 4 LAST 4 digits

        [XmlElement("AUTH-CODE", typeof(string))]
        public string AuthCode; //CHAR 7 Auth code

        [XmlElement("TICKET-COUNT", typeof(string))]
        public string TicketCount; //NUM 2 Max occurrence will be 99

        [XmlArray(ElementName = "TICKET-LIST")]
        [XmlArrayItem(ElementName = "TICKET-ITEM", Type = typeof(ConduentTicketItem))]

        public List<ConduentTicketItem> TicketList { get; set; }

        public ConduentNotificationRequest()
        {
            TicketList = new List<ConduentTicketItem>();
        }
        public ConduentNotificationRequest(string programName, string transId, string userId, string sessionId, 
            string password, string internetIndicator, string systemIndicator, string token, 
            string clientCode, string functionCode, string errorCode, string account, string paymentMethod,  string totalPaid, string processingFee, 
            string payDate, string payTime, string cardType, string cardNumber, string authCode, string ticketCount)
        {
            ProgramName = programName;
            TransId = transId;
            UserId = userId;
            SessionId = sessionId;
            Password = password;
            InternetIndicator = internetIndicator;
            //SystemIndicator = systemIndicator;
            Token = token;
            ClientCode = clientCode;
            FunctionCode = functionCode;
            ErrorCode = errorCode;
            Account = account;
            PaymentMethod = paymentMethod;
            TotalPaid = totalPaid;
            ProcessingFee = processingFee;
            PayDate = payDate;
            PayTime = payTime;
            CardType = cardType;
            CardNumber = cardNumber;
            AuthCode = authCode;
            TicketCount = ticketCount;
            TicketList = new List<ConduentTicketItem>();
        }
    }

    public class ConduentTicketItem
    {
        [XmlElement("FEE-TICK-IND", typeof(string))]
        public string FeeTicketIndicator; //CHAR 1 Fee Ticket Indicator (T is ticket and F is fee)

        [XmlElement("FEE-TYPE", typeof(string))]
        public string FeeType; //CHAR 3 Fee type: 001 = Boot Fee, 004 = Bad Check Fee, 005 = Enroll Fee, 006 = Late Fee

        [XmlElement("FEE-DATE", typeof(string))]
        public string FeeDate; //DATE 8 The format should be mm/DD/yyyy

        [XmlElement("FEE-TIME", typeof(string))]
        public string FeeTime; //TIME 4 The format should be hh:mma

        [XmlElement("TICKET-NUMBER", typeof(string))]
        public string TicketNumber; //CHAR 11 ticket number

        [XmlElement("AMOUNT-PAID", typeof(string))]
        public string AmountPaid; //NUM 7 Amount paid(for example 33.00)

        [XmlElement("ENTITY-NUMBER", typeof(string))]
        public string EntityNumber; //CHAR 8

        public ConduentTicketItem() { }

        public ConduentTicketItem(string feeTicketIndicator, 
                                    string feeType,
                                    string feeDate,
                                    string feeTime,
                                    string ticketNumber,
                                    string amountPaid,
                                    string entityNumber)
        {
            FeeTicketIndicator = feeTicketIndicator;
            FeeType = feeType;
            FeeDate = feeDate;
            FeeTime = feeTime;
            TicketNumber = ticketNumber;
            AmountPaid = amountPaid;
            EntityNumber = entityNumber;
        }

    }

    [ServiceContract(Namespace = "http://base.wsdl.wss.muni.acs.com")]
    public interface IPaymentNotification
    {
        [OperationContract(Action = "")]
        String CallPaymentNotification();
    }
    public class MockCall : IPaymentNotification
    {
        public string XmlRequest;
        public string Key;

        public MockCall() {

        }

        public MockCall(string xmlRequest, string key)
        {
            XmlRequest = xmlRequest;
            Key = key;
        }
         
        public String CallPaymentNotification()
        {
            try
            {
                PaymentListServiceClient _paymentListService = new PaymentListServiceClient();
                _paymentListService.Endpoint.Address = new EndpointAddress("https://qawssq.etimsportal.com/wss/services/PaymentListService");

                // Call the Web Service
                string responseXml = _paymentListService.sendPaymentDetails(XmlRequest,Key);
                return "passed";
            }
            catch (TimeoutException te)
            {
                return "timeout";
            }
            catch (Exception e)
            {
                string message = e.Message;
                return "failed";
            }
        }
    }
}
